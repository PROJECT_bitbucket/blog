@extends('layouts.app')

@section('title', 'Mein Account')

@section('content')
    <div class="container text-center mb-3">
        <a class="fas fa-plus-circle add-blog-icon" href="/blogs/create"></a>
    </div>
    @include('blogs.blogList')
@endsection
