<div class="container">
    <div class="row justify-content-center">
        @foreach ($blogs as $blog)
            <div class="col-8 mb-4">
                <a class="card shadow" href="/blogs/{{ $blog->id }}">
                    <h3 class="card-header">{{ $blog->title }}</h3>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="card-body" style="white-space: pre-wrap;">{{ $blog->content }}</div>
                        </li>
                        <li class="list-group-item text-secondary">
                            <div class="row">
                                <div class="col-6">{{ $blog->author->name }}</div>
                                <div class="col-6 text-right">{{ $blog->created_at }}</div>
                            </div>
                        </li>
                    </ul>
                </a>
            </div>
        @endforeach
    </div>
</div>
