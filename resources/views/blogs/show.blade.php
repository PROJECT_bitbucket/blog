@extends('layouts\app')

@section('title')
    {{ $blog->title }}
@endsection

@section('content')
    <div class="container">
        <div class="card shadow">
            <div class="card-header py-4">
                <h1 class="col-10 ml-1">{{ $blog->title }}</h1>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="card-body lead" style="white-space: pre-wrap;">{{ $blog->content }}</div>
                </li>
                <li class="list-group-item lead text-secondary">
                    <div class="row justify-content-between">
                        <div class="col-5 ml-3">{{ $blog->author->name }}</div>
                        <div class="col-5 mr-3 text-right">{{ $blog->created_at }}</div>
                    </div>
                </li>
            </ul>
        </div>
        @if (auth()->id() == $blog->author->id)
            <div class="row justify-content-around mt-5">
                <div class="col-3">
                    <a type="button" class="btn btn-primary btn-lg btn-block" href="/blogs/{{ $blog->id }}/edit">Editieren</a>
                </div>
                <div class="col-3">
                    <form method="POST" action="/blogs/{{ $blog->id }}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-lg btn-block">Löschen</button>
                    </form>
                </div>
            </div>
        @endif
    </div>
@endsection
