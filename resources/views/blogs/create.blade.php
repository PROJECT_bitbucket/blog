@extends('layouts.app')


@section('title', 'Blog erstellen')

@section('content')
    <div class="container">
        <div class="card">
            <h1 class="card-header">Erstelle einen neuen Blog</h1>
            <div class="card-body">
                <form method="POST" action="/blogs">
                    @csrf

                    <div class="form-group row mb-4">
                        <label for="titleInput" class="col-md-2"><h3>Titel:</h3></label>
                        <div class="col-md-5">
                            <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" id="titleInput" name="title" placeholder="Titel des Blogs" value="{{ old('title') }}" required>
                            <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                        </div>
                    </div>

                    <div class="form-group row mb-4">
                        <label for="contentInput" class="col-md-2"><h3>Inhalt:</h3></label>
                        <div class="col-md-9">
                            <textarea class="form-control {{ $errors->has('content') ? 'is-invalid' : '' }}" id="contentInput" name="content" rows="7" placeholder="Inhalt des Blogs" required>{{ old('content') }}</textarea>
                            <div class="invalid-feedback">{{ $errors->first('content') }}</div>
                        </div>
                    </div>

                    <div class="row">
                        <button type="submit" class="btn btn-lg btn-block btn-primary col-md-3 offset-md-2">Erstellen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
