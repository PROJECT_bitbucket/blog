@extends('layouts.app')

@section('title')
    Home
@endsection

@section('content')
    @include('blogs.blogList')
@endsection
