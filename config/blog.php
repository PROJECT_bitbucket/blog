<?php

return [
    'maxBlogsLoadedAtOnce' => 5,
    'maxShownContentLinebreaks' => 6,
    'maxShownContentChars' => 255
];
