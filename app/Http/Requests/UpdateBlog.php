<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBlog extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:50',
            'content' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Du musst hier den Titel deines Blogs angeben!',
            'title.min' => 'Der Titel deines Blogs muss mindestens 3 Zeichen enthalten!',
            'title.max' => 'Der Titel deines Blogs darf höchstens 50 Zeichen enthalten!',
            'content.required' => 'Du musst hier den Inhalt deines Blogs angeben!'
        ];
    }
}
