<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateBlog;

class BlogController extends Controller
{
    private $maxShownContentLinebreaks;       // Hier wird festgelegt, wie viele Zeilen des Blog-Inhalts bei der Auflistung von Blogs höchstens angezeigt werden soll
    private $maxShownContentChars;            // Hier wird festgelegt, wie viele Zeichen des Blog-Inhalts bei der Auflistung von Blogs höchstens angezeigt werden sollen

    public function __construct()
    {
        $this->middleware('auth')->except(['show', 'home']);

        $this->maxShownContentLinebreaks = config('blog.maxShownContentLinebreaks');
        $this->maxShownContentChars = config('blog.maxShownContentChars');
    }

    public function home()
    {
        $blogs = Blog::all()->sortByDesc('created_at');
        $blogs = $this->cutLongContent($blogs);

        return view('home', compact('blogs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = auth()->user()->blogs->sortByDesc('created_at');
        $blogs = $this->cutLongContent($blogs);

        return view('blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateBlog $request)
    {
        $attributes = $request->validated();
        $attributes['author_id'] = auth()->id();
        Blog::create($attributes);

        return redirect('/blogs');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        return view('blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        $this->authorize('update', $blog);

        return view('blogs.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlog $request, Blog $blog)
    {
        $this->authorize('update', $blog);
        $blog->update($request->validated());

        return redirect('/blogs/' . $blog->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $this->authorize('update', $blog);
        $blog->delete();

        return redirect('/blogs');
    }

    private function cutLongContent($blogs)
    {
        $blogs->map(function ($blog) {
            if (strlen($blog->content) > $this->maxShownContentChars) {
                $content = substr($blog->content, 0, $this->maxShownContentChars);
                $last_space = strrpos($content, ' ');
                $content = substr($content, 0, $last_space);
                $content .= '...';
                $blog->content = $content;
            }
            if (substr_count($blog->content, "\n") > $this->maxShownContentLinebreaks) {
                $content = $blog->content;
                $lastBreakPos = 0;
                $breakCount = 0;
                while ($breakCount != $this->maxShownContentLinebreaks) {
                    $lastBreakPos = strpos($content, "\n", $lastBreakPos) + 1;
                    $breakCount++;
                }
                $content = substr($content, 0, $lastBreakPos);
                $content .= '...';
                $blog->content = $content;
            }
            return $blog;
        });
        return $blogs;
    }
}
